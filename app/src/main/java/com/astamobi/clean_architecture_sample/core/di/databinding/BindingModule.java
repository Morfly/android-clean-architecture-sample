package com.astamobi.clean_architecture_sample.core.di.databinding;

import com.astamobi.clean_architecture_sample.core.view.databinding.DataBindingAdapters;
import com.astamobi.clean_architecture_sample.core.view.databinding.DataBindingAdaptersDefault;

import dagger.Module;
import dagger.Provides;

@Module
public class BindingModule {

    @Provides
    DataBindingAdapters provideBindingAdapters() {
        return new DataBindingAdaptersDefault();
    }

}
