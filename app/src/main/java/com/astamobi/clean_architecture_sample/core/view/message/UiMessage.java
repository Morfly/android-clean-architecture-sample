package com.astamobi.clean_architecture_sample.core.view.message;

/**
 * Interface for displaying ui messages (e.g. {@link ErrorType}
 */
public interface UiMessage {

    /**
     * @return Ui message string resource id
     */
    int getMessageResId();

}
