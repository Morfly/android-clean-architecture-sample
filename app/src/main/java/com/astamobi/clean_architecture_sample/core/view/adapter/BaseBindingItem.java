package com.astamobi.clean_architecture_sample.core.view.adapter;


import android.support.v7.widget.RecyclerView;

import com.astamobi.clean_architecture_sample.core.ViewModel;

/**
 * Item for {@link RecyclerView} {@link BindingAdapter}
 *
 * @param <VM> item viewModel type
 */
public abstract class BaseBindingItem<VM extends ViewModel> {

    protected VM viewModel;
    /**
     * Item type. Should contain item layout id
     */
    protected int itemType;

    /**
     * BR (binding resource) layout variable for item viewModel
     */
    protected int itemBindingVariable;


    /**
     * @param itemType should contain item layout id
     */
    public BaseBindingItem(VM viewModel, int itemType, int itemBindingVariable) {
        this.viewModel = viewModel;
        this.itemType = itemType;
        this.itemBindingVariable = itemBindingVariable;
    }


    // Getters
    public VM getViewModel() {
        return viewModel;
    }

    public int getItemType() {
        return itemType;
    }

    public int getItemBindingVariable() {
        return itemBindingVariable;
    }


    // Setters
    public void setViewModel(VM viewModel) {
        this.viewModel = viewModel;
    }


}
