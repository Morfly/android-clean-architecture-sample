package com.astamobi.clean_architecture_sample.core.view;


import android.app.ProgressDialog;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astamobi.clean_architecture_sample.R;
import com.astamobi.clean_architecture_sample.core.presenter.BasePresenter;
import com.astamobi.clean_architecture_sample.core.utils.UiUtils;
import com.astamobi.clean_architecture_sample.core.view.message.UiMessage;

import javax.inject.Inject;

public abstract class BaseFragment<P extends BasePresenter, B extends ViewDataBinding> extends Fragment implements BaseView {

    @Inject
    protected P presenter;
    protected B binding;
    private ProgressDialog progress;

    protected abstract int getLayoutId();

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.inject();
    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater, this.getLayoutId(), container, false);
        this.presenter.attachView(this);
        this.onCreateView(savedInstanceState);
        return this.binding.getRoot();
    }

    public abstract void onCreateView(Bundle savedInstanceState);

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    @Override
    public void setViewModel(int viewModelBindingResId, Object viewModel) {
        binding.setVariable(viewModelBindingResId, viewModel);
    }

    public abstract void inject();

    @Override
    public void showProgress() {
        getActivity().runOnUiThread(() -> {
            if (progress == null) {
                progress = new ProgressDialog(getActivity());
                progress.setCancelable(false);
                progress.setMessage(getString(R.string.message_loading));
            }
            if (!progress.isShowing()) {
                progress.show();
            }
        });
    }

    @Override
    public void hideProgress() {
        getActivity().runOnUiThread(() -> {
            if (progress != null && progress.isShowing()) {
                progress.dismiss();
            }
        });
    }

    @Override
    public void showMessage(String message) {
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) getActivity().findViewById(android.R.id.content)).getChildAt(0);
        UiUtils.showMessage(message, viewGroup);
    }

    @Override
    public void showMessage(int resId, Object... args) {
        Resources res = getResources();
        if (res != null) {
            String message = String.format(res.getString(resId), args);
            showMessage(message);
        }
    }

    @Override
    public void showMessage(UiMessage message, Object... args) {
        showMessage(message.getMessageResId(), args);
    }

    @Override
    public void showError(Throwable throwable) {
        // TODO implement error handler
        showMessage(throwable.getMessage());
    }

    @Override
    public void hideSoftKeyboard() {
        UiUtils.hideSoftKeyboard(this.getActivity());
    }

    @Override
    public void performBackAction() {
        getActivity().onBackPressed();
    }

}
