package com.astamobi.clean_architecture_sample.core.utils;


import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import rx.functions.Action0;

public final class UiUtils {

    private UiUtils() {
    }

    /**
     * Shows message with snackBar
     */
    public static void showMessage(String message, View view) {
        if (!TextUtils.isEmpty(message) && view != null) {
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    /**
     * Shows dialog with positive and negative custom actions
     */
    public static void showConfirmationDialog(Context context, String title,
                                              String positiveTitle, Action0 positiveAction,
                                              String negativeTitle, Action0 negativeAction) {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(title)
                    .setCancelable(true)
                    .setNegativeButton(negativeTitle, (dialog, which) -> {
                        if (negativeAction != null) {
                            negativeAction.call();
                        }
                    })
                    .setPositiveButton(positiveTitle, (dialog, which) -> {
                        if (positiveAction != null) {
                            positiveAction.call();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}
