package com.astamobi.clean_architecture_sample.core.di.databinding;


import android.databinding.DataBindingComponent;

import dagger.Component;

@Component(modules = BindingModule.class)
public interface BindingComponent extends DataBindingComponent {

}
