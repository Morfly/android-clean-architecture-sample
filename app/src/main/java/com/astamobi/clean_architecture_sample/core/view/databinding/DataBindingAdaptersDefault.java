package com.astamobi.clean_architecture_sample.core.view.databinding;


import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.astamobi.clean_architecture_sample.core.view.adapter.BaseBindingItem;
import com.astamobi.clean_architecture_sample.core.view.adapter.BindingAdapter;
import com.astamobi.clean_architecture_sample.core.view.image.CircleTransformation;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;

import java.util.Collection;

public class DataBindingAdaptersDefault implements DataBindingAdapters {

    @Override
    public void loadImage(ImageView view, String url, Drawable placeholder, Drawable error, boolean circle) {
        Transformation circleTransformation = null;
        if (circle) circleTransformation = new CircleTransformation();
        loadImage(view, url, placeholder, error, circleTransformation);
    }

    @Override
    public void loadImage(ImageView view, String url, Drawable placeholder, Drawable error, Transformation transformation) {
        RequestCreator builder = Picasso.with(view.getContext()).load(url);
        if (transformation != null) builder.transform(transformation);
        if (placeholder != null) builder.placeholder(placeholder);
        if (error != null) builder.error(error);
        builder.into(view);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <I extends BaseBindingItem> void setItems(RecyclerView recyclerView, Collection<I> items) {
        BindingAdapter<I> adapter = (BindingAdapter<I>) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.setItems(items);
        }
    }
}
