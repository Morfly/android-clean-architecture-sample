package com.astamobi.clean_architecture_sample.core.di.scope;


import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Dagger annotation
 * Custom scope for fragment
 */
@Scope
@Documented
@Retention(RUNTIME)
public @interface PerFragment {
}
