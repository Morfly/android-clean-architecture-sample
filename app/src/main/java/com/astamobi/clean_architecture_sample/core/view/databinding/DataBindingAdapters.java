package com.astamobi.clean_architecture_sample.core.view.databinding;


import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.astamobi.clean_architecture_sample.core.view.adapter.BaseBindingItem;
import com.squareup.picasso.Transformation;

import java.util.Collection;

/**
 * DataBinding instance adapters for layouts
 */
public interface DataBindingAdapters {

    /**
     * Displaying image by url with optional circle transformation
     */
    @BindingAdapter(value = {"bind:imageUrl", "bind:placeholder", "bind:error", "bind:circle"}, requireAll = false)
    void loadImage(ImageView view, String url, Drawable placeholder, Drawable error, boolean circle);

    /**
     * Displaying image by url with optional custom transformation
     */
    @BindingAdapter(value = {"bind:imageUrl", "bind:placeholder", "bind:error", "bind:transformation"}, requireAll = false)
    void loadImage(ImageView view, String url, Drawable placeholder, Drawable error, Transformation transformation);

    /**
     * Sets items to {@link RecyclerView} {@link com.astamobi.clean_architecture_sample.core.view.adapter.BindingAdapter}
     *
     * @param items list of items to display
     * @param <I>   item type
     */
    @BindingAdapter("bind:items")
    <I extends BaseBindingItem> void setItems(RecyclerView recyclerView, Collection<I> items);

}
