package com.astamobi.clean_architecture_sample.core.view.message;


import com.astamobi.clean_architecture_sample.R;

public enum ErrorType implements UiMessage {

    EMPTY_FIELDS(R.string.error_empty_fields),
    NO_INTERNET_CONNECTION(R.string.error_no_internet_connection);


    private int messageResId;

    ErrorType(int errorMessageResId) {
        this.messageResId = errorMessageResId;
    }

    @Override
    public int getMessageResId() {
        return messageResId;
    }

}
