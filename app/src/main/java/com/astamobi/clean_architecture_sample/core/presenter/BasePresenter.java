package com.astamobi.clean_architecture_sample.core.presenter;


import com.astamobi.clean_architecture_sample.core.ViewModel;
import com.astamobi.clean_architecture_sample.core.callback.Callback0;
import com.astamobi.clean_architecture_sample.core.utils.RxTransformers;
import com.astamobi.clean_architecture_sample.core.view.BaseView;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter<V extends BaseView, VM extends ViewModel> {

    private CompositeSubscription subscriptions;
    protected volatile V view;
    private VM viewModel;

    /**
     * Applies view model to the view
     */
    private void applyViewModel() {
        if (view != null) {
            view.setViewModel(view.getViewModelBindingResId(), this.viewModel);
        }
    }

    public VM getViewModel() {
        return viewModel;
    }

    public void setViewModel(VM viewModel) {
        this.viewModel = viewModel;
        applyViewModel();
    }

    public void attachView(V view) {
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        applyViewModel();
    }

    public void detachView() {
        view = null;
        if (subscriptions != null && !subscriptions.isUnsubscribed()) {
            subscriptions.unsubscribe();
        }
    }

    public void subscribe(Subscription observable) {
        this.subscriptions.add(observable);
    }

    /**
     * Lambdas for showing/hiding progress in rx chain
     * {@link RxTransformers#applyOnBeforeAndAfter(Callback0, Callback0)}
     */
    protected Callback0 showProgress = () -> {
        if (view != null) {
            view.showProgress();
        }
    };
    protected Callback0 hideProgress = () -> {
        if (view != null) {
            view.hideProgress();
        }
    };

    // If method is protected - rxJava doesn't want to handle errors. wtf???
    public void onError(Throwable throwable) {
        if (view != null) {
            view.showError(throwable);
        }
    }

}
