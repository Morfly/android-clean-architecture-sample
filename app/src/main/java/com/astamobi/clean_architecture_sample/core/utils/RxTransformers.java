package com.astamobi.clean_architecture_sample.core.utils;


import com.astamobi.clean_architecture_sample.core.callback.Callback0;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Helper class for common observable transformations
 */
public final class RxTransformers {

    /**
     * Apply standard {@link Schedulers}: io for {@link Observable}, ui for {@link Subscriber}
     */
    public static <T> Observable.Transformer<T, T> applyApiRequestSchedulers() {
        return tObservable -> tObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Run code before running observable {@link Observable} and after its termination
     *
     * @param before code that will run onSubscribe
     * @param after  code that will run afterTerminate. This will not call if Observable doesn't emit
     *               {@code onCompleted} or {@code onError}
     * @param <T>    {@link Object}
     * @return {@link Observable}
     */
    public static <T> Observable.Transformer<T, T> applyOnBeforeAndAfter(Callback0 before, Callback0 after) {
        return tObservable -> tObservable
                .doAfterTerminate(after::call)
                .doOnSubscribe(before::call);
    }

}
