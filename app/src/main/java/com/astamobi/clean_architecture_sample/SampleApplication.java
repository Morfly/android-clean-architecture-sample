package com.astamobi.clean_architecture_sample;


import android.app.Application;
import android.content.Context;
import android.databinding.DataBindingUtil;

import com.astamobi.clean_architecture_sample.core.di.databinding.BindingComponent;
import com.astamobi.clean_architecture_sample.core.di.databinding.DaggerBindingComponent;
import com.astamobi.clean_architecture_sample.core.view.databinding.DataBindingAdapters;
import com.astamobi.clean_architecture_sample.datalayer.di.ApplicationModule;
import com.astamobi.clean_architecture_sample.datalayer.di.PreferencesModule;
import com.astamobi.clean_architecture_sample.datalayer.di.RepositoryModule;
import com.astamobi.clean_architecture_sample.datalayer.di.RetrofitApiModule;

public class SampleApplication extends Application {

    private static SampleApplication instance;

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        /**
         * Init data binding default adapters with dagger
         * {@link DataBindingAdapters}
         * {@link BindingComponent}
         */
        DataBindingUtil.setDefaultComponent(DaggerBindingComponent.create());
    }

    // Dependency injection
    public static ApplicationComponent getComponent(Context context) {
        SampleApplication app = (SampleApplication) context.getApplicationContext();
        if (app.component == null) {
            app.component = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(get()))
                    .retrofitApiModule(new RetrofitApiModule(BuildConfig.API_ENDPOINT))
                    .preferencesModule(new PreferencesModule())
                    .repositoryModule(new RepositoryModule())
                    .build();
    }
        return app.component;
    }

    public static SampleApplication get() {
        return instance;
    }

}
