package com.astamobi.clean_architecture_sample.datalayer.preferences.profile;


import com.astamobi.clean_architecture_sample.datalayer.model.Sex;

public interface ProfileManager {

    void saveProfileData(ProfileModel profileData);

    void clearProfileData();

    boolean hasProfileData();

    String getId();

    String getFirstName();

    String getLastName();

    String getPhone();

    String getEmail();

    String getPicture();

    Sex getSex();

    String getToken();
}
