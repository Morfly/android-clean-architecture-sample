package com.astamobi.clean_architecture_sample.datalayer.di;


import com.astamobi.clean_architecture_sample.datalayer.network.RestApi;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitApiModule {

    private String baseUrl;

    public RetrofitApiModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        /**
         * {@link FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES}
         *
         * Using this naming policy with Gson will modify the Java Field name from its camel cased
         * form to a lower case field name where each word is separated by an underscore (_).
         *
         * <p>Here's a few examples of the form "Java Field Name" ---> "JSON Field Name":</p>
         * <ul>
         *   <li>someFieldName ---> some_field_name</li>
         *   <li>_someFieldName ---> _some_field_name</li>
         *   <li>aStringField ---> a_string_field</li>
         *   <li>aURL ---> a_u_r_l</li>
         * </ul>
         */
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.addInterceptor(logging);

        return okHttpClient.build();
    }

    @Provides
    @Singleton
    public RestApi provideRestApi(Gson gson, OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(RestApi.class);
    }
}
