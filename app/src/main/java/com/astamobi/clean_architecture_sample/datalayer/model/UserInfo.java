package com.astamobi.clean_architecture_sample.datalayer.model;

public class UserInfo {

    public String email;

    public String firstName;

    public String isChangePass;

    public String lastName;

    public String phone;

    public String picture;

    public String role;

    public String serviceId;

    public Sex sex;

    public String userId;
}
