package com.astamobi.clean_architecture_sample.datalayer.repository.service;


import com.astamobi.clean_architecture_sample.datalayer.model.CategoryModel;
import com.astamobi.clean_architecture_sample.datalayer.model.ServiceModel;
import com.astamobi.clean_architecture_sample.datalayer.network.response.ServiceDetailsResponse;

import java.util.List;

import rx.Observable;

public interface ServicesRepository {

    Observable<List<CategoryModel>> getServiceCategories();

    Observable<List<ServiceModel>> getServicesForCategory(int categoryId);

    Observable<List<ServiceModel>> searchServices(String searchQuery);

    Observable<ServiceDetailsResponse> getService(int serviceId);

}
