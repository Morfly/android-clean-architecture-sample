package com.astamobi.clean_architecture_sample.datalayer.di;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileManager;
import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileManagerImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferencesModule {

    public static final String PREF_NAME = "sample_app";

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Application application) {
        return application.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    ProfileManager provideProfileManager(SharedPreferences sharedPreferences) {
        return new ProfileManagerImpl(sharedPreferences);
    }
}
