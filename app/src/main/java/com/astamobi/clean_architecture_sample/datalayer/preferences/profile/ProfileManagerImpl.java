package com.astamobi.clean_architecture_sample.datalayer.preferences.profile;


import android.content.SharedPreferences;

import com.astamobi.clean_architecture_sample.datalayer.model.Sex;
import com.google.gson.Gson;

/**
 * Manager for storing current user profile data
 */
public final class ProfileManagerImpl implements ProfileManager {

    public static final String PROFILE_DATA = "profile_data";

    private SharedPreferences sharedPreferences;
    private volatile ProfileModel profileData;


    public ProfileManagerImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        this.profileData = loadProfileData();
    }

    /**
     * Saves JSON profile data into Shared Preferences
     *
     * @param profileModel - current user profile data
     */
    @Override
    public void saveProfileData(ProfileModel profileModel) {
        if (profileModel == null) clearProfileData();
        else {
            String json = new Gson().toJson(profileModel);
            sharedPreferences.edit().putString(PROFILE_DATA, json).apply();
            this.profileData = profileModel;
        }
    }

    /**
     * Retrieves profile data from storage
     *
     * @return current user profile data
     */
    private ProfileModel loadProfileData() {
        if (profileData == null || profileData.id == null || profileData.token == null) {
            String json = sharedPreferences.getString(PROFILE_DATA, null);
            profileData = json == null ? null : new Gson().fromJson(json, ProfileModel.class);
            if (profileData == null) profileData = new ProfileModel();
        }
        return profileData;
    }

    /**
     * Removes user data from storage
     */
    @Override
    public void clearProfileData() {
        sharedPreferences.edit().remove(PROFILE_DATA).apply();
        profileData = new ProfileModel();
    }

    @Override
    public boolean hasProfileData() {
        return profileData != null && profileData.id != null && profileData.token != null;
    }


    // Getters
    @Override
    public String getId() {
        return profileData.id;
    }

    @Override
    public String getFirstName() {
        return profileData.firstName;
    }

    @Override
    public String getLastName() {
        return profileData.lastName;
    }

    @Override
    public String getPhone() {
        return profileData.phone;
    }

    @Override
    public String getEmail() {
        return profileData.email;
    }

    @Override
    public String getPicture() {
        return profileData.picture;
    }

    @Override
    public Sex getSex() {
        return profileData.sex;
    }

    @Override
    public String getToken() {
        return profileData.token;
    }

}
