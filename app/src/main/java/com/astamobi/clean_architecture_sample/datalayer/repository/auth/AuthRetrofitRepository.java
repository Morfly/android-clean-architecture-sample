package com.astamobi.clean_architecture_sample.datalayer.repository.auth;


import com.astamobi.clean_architecture_sample.datalayer.network.RestApi;
import com.astamobi.clean_architecture_sample.datalayer.network.request.AuthRequest;
import com.astamobi.clean_architecture_sample.datalayer.network.request.RegisterRequest;
import com.astamobi.clean_architecture_sample.datalayer.network.response.AuthResponse;

import rx.Observable;

public class AuthRetrofitRepository implements AuthRepository {

    private RestApi api;

    public AuthRetrofitRepository(RestApi api) {
        this.api = api;
    }

    @Override
    public Observable<AuthResponse> login(AuthRequest authRequest) {
        return api.login(authRequest);
    }

    @Override
    public Observable<AuthResponse> register(RegisterRequest request) {
        return api.register(request);
    }
}
