package com.astamobi.clean_architecture_sample.datalayer.model;


public class ServiceModel {

    public int id;

    public String name;

    public String description;

    public String category;

    public String picture;

}
