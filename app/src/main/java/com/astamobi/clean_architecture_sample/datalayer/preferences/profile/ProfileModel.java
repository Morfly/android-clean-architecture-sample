package com.astamobi.clean_architecture_sample.datalayer.preferences.profile;


import com.astamobi.clean_architecture_sample.datalayer.model.Sex;

/**
 * Uses by {@link ProfileManager}
 */
public class ProfileModel {

    public String id;

    public String firstName;

    public String lastName;

    public String phone;

    public String email;

    public String picture;

    public Sex sex;

    public String token;

}
