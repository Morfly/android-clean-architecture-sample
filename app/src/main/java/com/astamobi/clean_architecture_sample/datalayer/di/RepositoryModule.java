package com.astamobi.clean_architecture_sample.datalayer.di;


import com.astamobi.clean_architecture_sample.datalayer.network.RestApi;
import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileManager;
import com.astamobi.clean_architecture_sample.datalayer.repository.auth.AuthRepository;
import com.astamobi.clean_architecture_sample.datalayer.repository.auth.AuthRetrofitRepository;
import com.astamobi.clean_architecture_sample.datalayer.repository.service.ServicesRepository;
import com.astamobi.clean_architecture_sample.datalayer.repository.service.ServicesRetrofitRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    AuthRepository provideAuthRepository(RestApi restApi, ProfileManager profileManager) {
        return new AuthRetrofitRepository(restApi);
    }

    @Provides
    @Singleton
    ServicesRepository provideServicesRepository(RestApi restApi, ProfileManager profileManager) {
        return new ServicesRetrofitRepository(restApi, profileManager);
    }
}
