package com.astamobi.clean_architecture_sample.datalayer.network;


import com.astamobi.clean_architecture_sample.datalayer.model.CategoryModel;
import com.astamobi.clean_architecture_sample.datalayer.model.ServiceModel;
import com.astamobi.clean_architecture_sample.datalayer.network.request.AuthRequest;
import com.astamobi.clean_architecture_sample.datalayer.network.request.RegisterRequest;
import com.astamobi.clean_architecture_sample.datalayer.network.response.AuthResponse;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface RestApi {

    @POST("login")
    Observable<AuthResponse> login(@Body AuthRequest authRequest);

    @POST("registerUser")
    Observable<AuthResponse> register(@Body RegisterRequest registerRequest);

    @GET("getServicesCategories")
    Observable<List<CategoryModel>> getServiceCategories(@Header("token") String token);

    @GET("getServicesList")
    Observable<List<ServiceModel>> getServicesList(@Header("token") String token, @Query("category_id") int categoryId);

    @GET("servicesListSearch")
    Observable<List<ServiceModel>> searchServices(@Header("token") String token, @Query("search") String searchQuery);

}
