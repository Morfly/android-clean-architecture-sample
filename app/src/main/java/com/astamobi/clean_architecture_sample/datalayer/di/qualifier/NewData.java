package com.astamobi.clean_architecture_sample.datalayer.di.qualifier;


import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * Dagger annotation
 * Custom qualifier for retrieving new data from network
 */
@Qualifier
@Documented
@Retention(RUNTIME)
public @interface NewData {
}
