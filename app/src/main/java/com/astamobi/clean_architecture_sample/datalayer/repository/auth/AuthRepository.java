package com.astamobi.clean_architecture_sample.datalayer.repository.auth;


import com.astamobi.clean_architecture_sample.datalayer.network.request.AuthRequest;
import com.astamobi.clean_architecture_sample.datalayer.network.request.RegisterRequest;
import com.astamobi.clean_architecture_sample.datalayer.network.response.AuthResponse;

import rx.Observable;

public interface AuthRepository {

    Observable<AuthResponse> login(AuthRequest authRequest);

    Observable<AuthResponse> register(RegisterRequest request);
}
