package com.astamobi.clean_architecture_sample.datalayer.repository;


import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileManager;

/**
 * Inherit this repository for accessing {@link ProfileManager}
 */
public abstract class AuthSessionRepository {

    public ProfileManager profileManager;

    public AuthSessionRepository(ProfileManager profileManager) {
        this.profileManager = profileManager;
    }
}
