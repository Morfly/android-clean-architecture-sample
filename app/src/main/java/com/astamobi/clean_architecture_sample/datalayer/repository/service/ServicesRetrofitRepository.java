package com.astamobi.clean_architecture_sample.datalayer.repository.service;


import com.astamobi.clean_architecture_sample.datalayer.model.CategoryModel;
import com.astamobi.clean_architecture_sample.datalayer.model.ServiceModel;
import com.astamobi.clean_architecture_sample.datalayer.network.RestApi;
import com.astamobi.clean_architecture_sample.datalayer.network.response.ServiceDetailsResponse;
import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileManager;
import com.astamobi.clean_architecture_sample.datalayer.repository.AuthSessionRepository;

import java.util.List;

import rx.Observable;

public class ServicesRetrofitRepository extends AuthSessionRepository implements ServicesRepository {

    private RestApi api;

    public ServicesRetrofitRepository(RestApi api, ProfileManager profileManager) {
        super(profileManager);
        this.api = api;
    }

    @Override
    public Observable<List<CategoryModel>> getServiceCategories() {
        return api.getServiceCategories(profileManager.getToken());
    }

    @Override
    public Observable<List<ServiceModel>> getServicesForCategory(int categoryId) {
        return api.getServicesList(profileManager.getToken(), categoryId);
    }

    @Override
    public Observable<List<ServiceModel>> searchServices(String searchQuery) {
        return api.searchServices(profileManager.getToken(), searchQuery);
    }

    @Override
    public Observable<ServiceDetailsResponse> getService(int serviceId) {
        return Observable.empty();
    }
}
