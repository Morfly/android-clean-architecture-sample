package com.astamobi.clean_architecture_sample.datalayer.model;


public enum Sex {
    male, female
}
