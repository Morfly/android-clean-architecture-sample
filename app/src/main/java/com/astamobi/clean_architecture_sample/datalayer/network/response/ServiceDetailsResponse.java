package com.astamobi.clean_architecture_sample.datalayer.network.response;


import com.astamobi.clean_architecture_sample.datalayer.model.LocationModel;
import com.astamobi.clean_architecture_sample.datalayer.model.WorkScheduleModel;

import java.util.List;

public class ServiceDetailsResponse {

    public int id;

    public String name;

    public String description;

    public String category;

    public String picture;

    public String address;

    public String phone;

    public List<WorkScheduleModel> workSchedule;

    public LocationModel location;

}
