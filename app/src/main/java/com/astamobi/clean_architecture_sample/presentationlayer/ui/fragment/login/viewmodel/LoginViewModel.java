package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.viewmodel;


import android.databinding.ObservableField;

import com.astamobi.clean_architecture_sample.core.ViewModel;


public class LoginViewModel implements ViewModel {

    /**
     * Fields are two-way bound with appropriate EditText fields in layout
     */

    public final ObservableField<String> email = new ObservableField<>("");

    public final ObservableField<String> password = new ObservableField<>("");

}
