package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.presenter;


import com.astamobi.clean_architecture_sample.core.presenter.BasePresenter;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.view.LoginView;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.viewmodel.LoginViewModel;

public abstract class LoginPresenter extends BasePresenter<LoginView, LoginViewModel> {

    public abstract void onLoginClick();

    public abstract void onRegisterClick();

    public abstract void onFbLoginClick();

    public abstract void onVkLoginClick();

    public abstract void onGpLoginClick();

}
