package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.di;


import com.astamobi.clean_architecture_sample.core.di.scope.PerFragment;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.ServicesFragment;

import dagger.Subcomponent;

@PerFragment
@Subcomponent(modules = {ServicesModule.class})
public interface ServicesComponent {

    void inject(ServicesFragment servicesFragment);
}
