package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.di;


import com.astamobi.clean_architecture_sample.core.di.scope.PerFragment;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.view.LoginFragment;

import dagger.Subcomponent;

@PerFragment
@Subcomponent(modules = {LoginModule.class})
public interface LoginComponent {

    void inject(LoginFragment loginFragment);
}
