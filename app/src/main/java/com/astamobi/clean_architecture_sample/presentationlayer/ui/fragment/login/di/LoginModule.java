package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.di;


import com.astamobi.clean_architecture_sample.core.di.scope.PerFragment;
import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileManager;
import com.astamobi.clean_architecture_sample.datalayer.repository.auth.AuthRepository;
import com.astamobi.clean_architecture_sample.domainlayer.interactor.auth.LoginInteractor;
import com.astamobi.clean_architecture_sample.domainlayer.interactor.auth.LoginUseCase;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.presenter.LoginPresenter;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.presenter.LoginPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    @Provides
    @PerFragment
    LoginInteractor provideLoginInteractor(AuthRepository authRepository, ProfileManager profileManager) {
        return new LoginUseCase(authRepository, profileManager);
    }

    @Provides
    @PerFragment
    LoginPresenter provideLoginPresenter(LoginInteractor loginInteractor) {
        return new LoginPresenterImpl(loginInteractor);
    }
}
