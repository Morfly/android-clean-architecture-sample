package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.astamobi.clean_architecture_sample.BR;
import com.astamobi.clean_architecture_sample.R;
import com.astamobi.clean_architecture_sample.SampleApplication;
import com.astamobi.clean_architecture_sample.core.view.BaseFragment;
import com.astamobi.clean_architecture_sample.core.view.adapter.BindingAdapter;
import com.astamobi.clean_architecture_sample.databinding.FragmentServicesBinding;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.di.ServicesModule;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.presenter.ServicesPresenter;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.adapter.ServiceItem;


public class ServicesFragment extends BaseFragment<ServicesPresenter, FragmentServicesBinding> implements ServicesView {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_services;
    }

    @Override
    public int getViewModelBindingResId() {
        return BR.viewModel;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        BindingAdapter<ServiceItem> adapter = new BindingAdapter<>();
        adapter.setOnItemClickListener(presenter::onServiceItemClick);
        binding.listServices.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.listServices.setAdapter(adapter);

        presenter.getCategories();
    }

    @Override
    public void inject() {
        SampleApplication.getComponent(getActivity())
                .plus(new ServicesModule())
                .inject(this);
    }
}
