package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.astamobi.clean_architecture_sample.BR;
import com.astamobi.clean_architecture_sample.core.ViewModel;

public class ServiceItemViewModel extends BaseObservable implements ViewModel {

    private String name;

    private String description;

    public String pictureUrl;


    public ServiceItemViewModel(String name, String description, String pictureUrl) {
        this.name = name;
        this.description = description;
        this.pictureUrl = pictureUrl;
    }


    // Getters
    @Bindable
    public String getName() {
        return name;
    }

    @Bindable
    public String getDescription() {
        return description;
    }


    //Setters
    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }
}
