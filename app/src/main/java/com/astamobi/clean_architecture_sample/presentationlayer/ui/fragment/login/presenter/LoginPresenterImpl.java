package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.presenter;


import android.text.TextUtils;

import com.astamobi.clean_architecture_sample.core.utils.RxTransformers;
import com.astamobi.clean_architecture_sample.core.view.message.ErrorType;
import com.astamobi.clean_architecture_sample.datalayer.network.request.AuthRequest;
import com.astamobi.clean_architecture_sample.domainlayer.interactor.auth.LoginInteractor;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.viewmodel.LoginViewModel;


public class LoginPresenterImpl extends LoginPresenter {

    private LoginInteractor loginInteractor;

    public LoginPresenterImpl(LoginInteractor login) {
        this.loginInteractor = login;
        // Set viewModel to the layout through dataBinding
        setViewModel(getMockViewModel());
    }

    @Override
    public void onLoginClick() {
        String email = getViewModel().email.get();
        String password = getViewModel().password.get();
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            AuthRequest authRequest = new AuthRequest();
            authRequest.login = email;
            authRequest.password = password;
            subscribe(loginInteractor.login(authRequest)
                    .compose(RxTransformers.applyApiRequestSchedulers())
                    .compose(RxTransformers.applyOnBeforeAndAfter(showProgress, hideProgress))
                    .subscribe(r -> {
                    }, this::onError, this::showNextScreen));
        } else this.view.showMessage(ErrorType.EMPTY_FIELDS);

    }

    @Override
    public void onRegisterClick() {
        // Show register screen
    }

    @Override
    public void onFbLoginClick() {
        // Login via facebook
    }

    @Override
    public void onVkLoginClick() {
        // Login via vkontakte
    }

    @Override
    public void onGpLoginClick() {
        // Login via google plus
    }


    private void showNextScreen() {
        // Opens some next screen (e.g. Services)
        view.showServices();
    }

    /**
     * Debug stub with existing account data
     */
    private LoginViewModel getMockViewModel() {
        LoginViewModel viewModel = new LoginViewModel();
        viewModel.email.set("mutest@test.com");
        viewModel.password.set("qwerty");
        return viewModel;
    }

}
