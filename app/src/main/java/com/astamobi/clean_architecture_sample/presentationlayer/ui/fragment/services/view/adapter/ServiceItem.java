package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.adapter;


import com.astamobi.clean_architecture_sample.R;
import com.astamobi.clean_architecture_sample.core.ViewModel;
import com.astamobi.clean_architecture_sample.core.view.adapter.BaseBindingItem;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel.ServicesMapperImpl;

public class ServiceItem<VM extends ViewModel> extends BaseBindingItem<VM> {

    public static final int ITEM_SERVICE = R.layout.item_service;
    /**
     * If there were several items in the recyclerView, list them here:
     * e.g. public static final int ITEM_ANOTHER = R.layout.item_another;
     * ...
     * These item types are used in mappers. (e.g. {@link ServicesMapperImpl})
     */

    public int id;

    public String category;

    public ServiceItem(VM viewModel, int itemType, int bindingVariable) {
        super(viewModel, itemType, bindingVariable);
    }
}
