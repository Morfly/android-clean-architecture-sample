package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.di;


import com.astamobi.clean_architecture_sample.core.di.scope.PerFragment;
import com.astamobi.clean_architecture_sample.datalayer.repository.service.ServicesRepository;
import com.astamobi.clean_architecture_sample.domainlayer.interactor.service.GetServiceListInteractor;
import com.astamobi.clean_architecture_sample.domainlayer.interactor.service.GetServiceListUseCase;
import com.astamobi.clean_architecture_sample.domainlayer.interactor.service.SearchServicesInteractor;
import com.astamobi.clean_architecture_sample.domainlayer.interactor.service.SearchServicesUseCase;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.presenter.ServicesPresenter;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.presenter.ServicesPresenterImpl;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel.ServicesMapper;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel.ServicesMapperImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ServicesModule {

    @Provides
    @PerFragment
    GetServiceListInteractor provideGetServiceListInteractor(ServicesRepository repository) {
        return new GetServiceListUseCase(repository);
    }

    @Provides
    @PerFragment
    SearchServicesInteractor provideSearchServicesInteractor(ServicesRepository repository) {
        return new SearchServicesUseCase(repository);
    }

    @Provides
    @PerFragment
    ServicesMapper provideServicesMapper() {
        return new ServicesMapperImpl();
    }

    @Provides
    @PerFragment
    ServicesPresenter provideServicesPresenter(GetServiceListInteractor getServiceList,
                                               SearchServicesInteractor searchServices,
                                               ServicesMapper mapper) {
        return new ServicesPresenterImpl(getServiceList, searchServices, mapper);
    }
}
