package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view;


import com.astamobi.clean_architecture_sample.core.view.BaseView;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.adapter.ServiceItem;

import java.util.List;

public interface ServicesView extends BaseView {

}
