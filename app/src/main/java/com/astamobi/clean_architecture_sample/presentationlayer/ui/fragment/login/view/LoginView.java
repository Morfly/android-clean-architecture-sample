package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.view;


import com.astamobi.clean_architecture_sample.core.view.BaseView;

public interface LoginView extends BaseView {

    void showServices();
}
