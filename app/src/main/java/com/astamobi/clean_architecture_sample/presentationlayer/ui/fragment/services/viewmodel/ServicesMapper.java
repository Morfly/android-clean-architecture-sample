package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel;


import com.astamobi.clean_architecture_sample.datalayer.model.ServiceModel;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.adapter.ServiceItem;

import java.util.List;

public interface ServicesMapper {

    List<ServiceItem> mapServices(List<ServiceModel> response);
}
