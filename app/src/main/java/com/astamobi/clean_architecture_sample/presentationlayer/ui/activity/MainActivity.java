package com.astamobi.clean_architecture_sample.presentationlayer.ui.activity;

import android.os.Bundle;

import com.astamobi.clean_architecture_sample.R;
import com.astamobi.clean_architecture_sample.core.view.BaseActivity;
import com.astamobi.clean_architecture_sample.databinding.ActivityMainBinding;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.view.LoginFragment;

public class MainActivity extends BaseActivity<ActivityMainBinding> {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        replaceFragment(new LoginFragment(), R.id.container, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }
}
