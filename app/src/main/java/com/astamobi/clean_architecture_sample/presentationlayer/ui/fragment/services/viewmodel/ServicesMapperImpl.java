package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel;


import com.android.databinding.library.baseAdapters.BR;
import com.astamobi.clean_architecture_sample.datalayer.model.ServiceModel;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.adapter.ServiceItem;

import java.util.ArrayList;
import java.util.List;

public class ServicesMapperImpl implements ServicesMapper {

    @Override
    public List<ServiceItem> mapServices(List<ServiceModel> response) {
        List<ServiceItem> services = new ArrayList<>();
        for (ServiceModel responseItem : response) {

            ServiceItemViewModel itemViewModel = new ServiceItemViewModel(responseItem.name,
                    responseItem.description,
                    responseItem.picture);

            ServiceItem<ServiceItemViewModel> serviceItem = new ServiceItem<>(
                    itemViewModel, ServiceItem.ITEM_SERVICE, BR.viewModel);

            serviceItem.id = responseItem.id;
            serviceItem.category = responseItem.category;
            services.add(serviceItem);
        }
        return services;
    }
}
