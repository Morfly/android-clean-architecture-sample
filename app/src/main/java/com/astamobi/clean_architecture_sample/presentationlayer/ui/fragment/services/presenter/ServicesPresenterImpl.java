package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.presenter;


import com.astamobi.clean_architecture_sample.core.utils.RxTransformers;
import com.astamobi.clean_architecture_sample.domainlayer.interactor.service.GetServiceListInteractor;
import com.astamobi.clean_architecture_sample.domainlayer.interactor.service.SearchServicesInteractor;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.adapter.ServiceItem;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel.ServiceItemViewModel;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel.ServicesMapper;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel.ServicesViewModel;

import java.util.List;

import rx.Observable;

public class ServicesPresenterImpl extends ServicesPresenter {

    private final GetServiceListInteractor getServiceList;
    private final SearchServicesInteractor searchServices;
    private final ServicesMapper mapper;

    public ServicesPresenterImpl(GetServiceListInteractor getServiceList,
                                 SearchServicesInteractor searchServices,
                                 ServicesMapper mapper) {
        this.getServiceList = getServiceList;
        this.searchServices = searchServices;
        this.mapper = mapper;

        setViewModel(new ServicesViewModel());
    }

    @Override
    public void getCategories() {
        subscribe(getServiceList.getServiceCategories()
                .flatMap(Observable::from)
                .flatMap(category -> getServiceList.getServicesForCategory(category.id))
                .map(mapper::mapServices)
                .compose(RxTransformers.applyApiRequestSchedulers())
                .compose(RxTransformers.applyOnBeforeAndAfter(showProgress, hideProgress))
                .subscribe(getViewModel().services::addAll, this::onError));
    }

    @Override
    public void onSearchServices(String searchQuery) {
        subscribe(searchServices.searchServices(searchQuery)
                .map(mapper::mapServices)
                .compose(RxTransformers.applyApiRequestSchedulers())
                .compose(RxTransformers.applyOnBeforeAndAfter(showProgress, hideProgress))
                .subscribe(this::setServices, this::onError));
    }

    @Override
    public void onServiceItemClick(ServiceItem item) {
        if (item.getViewModel() instanceof ServiceItemViewModel) {
            view.showMessage(((ServiceItemViewModel) item.getViewModel()).getDescription() + " " + item.id);
        }
    }

    private void setServices(List<ServiceItem> items) {
        getViewModel().services.clear();
        getViewModel().services.addAll(items);
    }
}
