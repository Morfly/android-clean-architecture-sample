package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel;


import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.v7.widget.RecyclerView;

import com.astamobi.clean_architecture_sample.core.ViewModel;
import com.astamobi.clean_architecture_sample.core.view.adapter.BindingAdapter;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.adapter.ServiceItem;

public class ServicesViewModel implements ViewModel {

    /**
     * List for binding with {@link RecyclerView} {@link BindingAdapter}
     */
//    @Bindable
    public ObservableList<ServiceItem> services = new ObservableArrayList<>();

}
