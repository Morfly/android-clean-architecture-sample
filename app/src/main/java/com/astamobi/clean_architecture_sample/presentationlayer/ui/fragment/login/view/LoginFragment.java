package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.view;


import android.os.Bundle;

import com.astamobi.clean_architecture_sample.BR;
import com.astamobi.clean_architecture_sample.R;
import com.astamobi.clean_architecture_sample.SampleApplication;
import com.astamobi.clean_architecture_sample.core.view.BaseActivity;
import com.astamobi.clean_architecture_sample.core.view.BaseFragment;
import com.astamobi.clean_architecture_sample.databinding.FragmentLoginBinding;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.di.LoginModule;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.presenter.LoginPresenter;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.ServicesFragment;


public class LoginFragment extends BaseFragment<LoginPresenter, FragmentLoginBinding> implements LoginView {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    public int getViewModelBindingResId() {
        return BR.viewModel;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        binding.setPresenter(this.presenter);
    }

    @Override
    public void inject() {
        SampleApplication.getComponent(getActivity())
                .plus(new LoginModule())
                .inject(this);
    }

    @Override
    public void showServices() {
        ((BaseActivity) getActivity()).replaceFragment(new ServicesFragment(), R.id.container, true);
    }
}