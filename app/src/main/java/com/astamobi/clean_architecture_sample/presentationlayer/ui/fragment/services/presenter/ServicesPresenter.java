package com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.presenter;


import com.astamobi.clean_architecture_sample.core.presenter.BasePresenter;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.ServicesView;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.view.adapter.ServiceItem;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.viewmodel.ServicesViewModel;

public abstract class ServicesPresenter extends BasePresenter<ServicesView, ServicesViewModel> {

    public abstract void getCategories();

    public abstract void onSearchServices(String searchQuery);

    public abstract void onServiceItemClick(ServiceItem item);
}
