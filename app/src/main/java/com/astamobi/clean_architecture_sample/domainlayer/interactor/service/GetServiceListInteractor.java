package com.astamobi.clean_architecture_sample.domainlayer.interactor.service;


import com.astamobi.clean_architecture_sample.datalayer.model.CategoryModel;
import com.astamobi.clean_architecture_sample.datalayer.model.ServiceModel;

import java.util.List;

import rx.Observable;

public interface GetServiceListInteractor {

    Observable<List<CategoryModel>> getServiceCategories();

    Observable<List<ServiceModel>> getServicesForCategory(int categoryId);
}
