package com.astamobi.clean_architecture_sample.domainlayer.interactor.auth;


import com.astamobi.clean_architecture_sample.datalayer.network.request.AuthRequest;

import rx.Observable;

public interface LoginInteractor {

    Observable<Void> login(AuthRequest authRequest);
}
