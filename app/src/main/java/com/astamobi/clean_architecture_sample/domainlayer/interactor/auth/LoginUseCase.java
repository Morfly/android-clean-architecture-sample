package com.astamobi.clean_architecture_sample.domainlayer.interactor.auth;


import com.astamobi.clean_architecture_sample.datalayer.network.request.AuthRequest;
import com.astamobi.clean_architecture_sample.datalayer.network.response.AuthResponse;
import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileManager;
import com.astamobi.clean_architecture_sample.datalayer.repository.auth.AuthRepository;

import rx.Observable;

public class LoginUseCase implements LoginInteractor {

    private AuthRepository repository;
    private ProfileManager profileManager;

    public LoginUseCase(AuthRepository repository, ProfileManager profileManager) {
        this.profileManager = profileManager;
        this.repository = repository;
    }

    @Override
    public Observable<Void> login(AuthRequest authRequest) {
        return repository.login(authRequest)
                .doOnNext(response -> profileManager.saveProfileData(response))
                .flatMap(response -> Observable.empty());
    }
}
