package com.astamobi.clean_architecture_sample.domainlayer.interactor.auth;


import com.astamobi.clean_architecture_sample.datalayer.network.request.RegisterRequest;
import com.astamobi.clean_architecture_sample.datalayer.network.response.AuthResponse;

import rx.Observable;

public interface RegisterInteractor {

    Observable<AuthResponse> register(RegisterRequest request);
}
