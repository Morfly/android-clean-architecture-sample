package com.astamobi.clean_architecture_sample.domainlayer.interactor.service;


import com.astamobi.clean_architecture_sample.datalayer.model.ServiceModel;
import com.astamobi.clean_architecture_sample.datalayer.repository.service.ServicesRepository;

import java.util.List;

import rx.Observable;

public class SearchServicesUseCase implements SearchServicesInteractor {

    private ServicesRepository repository;

    public SearchServicesUseCase(ServicesRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<List<ServiceModel>> searchServices(String searchQuery) {
        return repository.searchServices(searchQuery);
    }
}
