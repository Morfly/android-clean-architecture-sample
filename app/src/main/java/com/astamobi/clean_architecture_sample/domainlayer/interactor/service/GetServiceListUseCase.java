package com.astamobi.clean_architecture_sample.domainlayer.interactor.service;


import com.astamobi.clean_architecture_sample.datalayer.model.CategoryModel;
import com.astamobi.clean_architecture_sample.datalayer.model.ServiceModel;
import com.astamobi.clean_architecture_sample.datalayer.repository.service.ServicesRepository;

import java.util.List;

import rx.Observable;

public class GetServiceListUseCase implements GetServiceListInteractor {

    private ServicesRepository repository;

    public GetServiceListUseCase(ServicesRepository repository) {
        this.repository = repository;
    }


    @Override
    public Observable<List<CategoryModel>> getServiceCategories() {
        return repository.getServiceCategories();
    }

    @Override
    public Observable<List<ServiceModel>> getServicesForCategory(int categoryId) {
        return repository.getServicesForCategory(categoryId);
    }
}
