package com.astamobi.clean_architecture_sample.domainlayer.interactor.auth;


import com.astamobi.clean_architecture_sample.datalayer.network.request.RegisterRequest;
import com.astamobi.clean_architecture_sample.datalayer.network.response.AuthResponse;
import com.astamobi.clean_architecture_sample.datalayer.repository.auth.AuthRepository;

import rx.Observable;

public class RegisterUseCase implements RegisterInteractor {

    private AuthRepository repository;

    public RegisterUseCase(AuthRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<AuthResponse> register(RegisterRequest request) {
        return repository.register(request);
    }
}
