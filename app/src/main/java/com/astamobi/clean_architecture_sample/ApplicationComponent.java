package com.astamobi.clean_architecture_sample;


import com.astamobi.clean_architecture_sample.datalayer.di.ApplicationModule;
import com.astamobi.clean_architecture_sample.datalayer.di.PreferencesModule;
import com.astamobi.clean_architecture_sample.datalayer.di.RepositoryModule;
import com.astamobi.clean_architecture_sample.datalayer.di.RetrofitApiModule;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.di.LoginComponent;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.login.di.LoginModule;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.di.ServicesComponent;
import com.astamobi.clean_architecture_sample.presentationlayer.ui.fragment.services.di.ServicesModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        RetrofitApiModule.class,
        PreferencesModule.class,
        RepositoryModule.class
})
public interface ApplicationComponent {

    // SubComponents

    LoginComponent plus(LoginModule loginModule);

    ServicesComponent plus(ServicesModule servicesModule);

}
