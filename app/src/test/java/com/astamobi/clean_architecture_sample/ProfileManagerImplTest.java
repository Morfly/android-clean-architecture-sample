package com.astamobi.clean_architecture_sample;


import android.content.Context;
import android.content.SharedPreferences;

import com.astamobi.clean_architecture_sample.datalayer.model.Sex;
import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileManager;
import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileManagerImpl;
import com.astamobi.clean_architecture_sample.datalayer.preferences.profile.ProfileModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Unit test for {@link ProfileManagerImpl}
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class ProfileManagerImplTest {

    private static final String MOCK_ID = "id";
    private static final String MOCK_TOKEN = "token";
    private static final String MOCK_FIRST_NAME = "Banana";
    private static final Sex MOCK_SEX = Sex.female;

    private ProfileManager profileManager;
    private ProfileModel mockProfileData;

    @Before
    public void initProfileManager() {
        SharedPreferences preferences = RuntimeEnvironment
                .application
                .getSharedPreferences(ProfileManagerImpl.PROFILE_DATA, Context.MODE_PRIVATE);
        profileManager = new ProfileManagerImpl(preferences);
    }

    @Before
    public void initMockProfileData() {
        mockProfileData = new ProfileModel();
        mockProfileData.id = MOCK_ID;
        mockProfileData.firstName = MOCK_FIRST_NAME;
        mockProfileData.token = MOCK_TOKEN;
        mockProfileData.sex = MOCK_SEX;
    }

    private void assertEmptyProfileManager() {
        assertNull(profileManager.getId());
        assertNull(profileManager.getFirstName());
        assertNull(profileManager.getToken());
        assertNull(profileManager.getSex());
    }

    private void assertMockedProfileManager() {
        assertEquals(MOCK_ID, profileManager.getId());
        assertEquals(MOCK_FIRST_NAME, profileManager.getFirstName());
        assertEquals(MOCK_TOKEN, profileManager.getToken());
        assertEquals(MOCK_SEX, profileManager.getSex());
    }

    /**
     * Test if saving new profile data is correct
     */
    @Test
    public void testSaveProfile() throws Exception {
        assertEmptyProfileManager();

        profileManager.saveProfileData(mockProfileData);
        assertMockedProfileManager();
    }

    /**
     * Test if clearing profile data is correct
     */
    @Test
    public void testClearProfile() throws Exception {
        assertEmptyProfileManager();

        profileManager.saveProfileData(mockProfileData);
        assertMockedProfileManager();

        profileManager.clearProfileData();
        assertEmptyProfileManager();
    }

    /**
     * Testing case when saving null as profile data
     * Should return not-null empty ProfileManager object
     */
    @Test
    public void testSaveNull() throws Exception {
        profileManager.saveProfileData(null);
        assertEmptyProfileManager();
    }

    /**
     * Test if saving new profile data over existing is correct
     */
    @Test
    public void testReSaveProfile() throws Exception {
        assertEmptyProfileManager();

        profileManager.saveProfileData(mockProfileData);
        assertMockedProfileManager();

        ProfileModel newMockProfileData = new ProfileModel();
        String newMockId = MOCK_ID + "1";
        newMockProfileData.id = newMockId;
        profileManager.saveProfileData(newMockProfileData);
        assertEquals(newMockId, profileManager.getId());
    }

}
